﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using AnyThingApp.Models;
using Microsoft.VisualBasic.FileIO;

namespace AnyThingApp.Controllers
{
    public class CSVController : ApiController
    {
        //
        // GET: /api/CSV
        public AnythingDataModel GetDataFromCSVFile()
        {
            return ParseCSVDataToTable();
        }

        /// <summary>
        /// Read data from CSV file and put it in the table structure
        /// </summary>
        /// <returns>AnythingDataModel</returns>
        private static AnythingDataModel ParseCSVDataToTable()
        {
            var anythingDataModel = new AnythingDataModel();
            var tableStructure = new TableStructure();

            try
            {
                var path = GetFilePath();
                using (var parser = new TextFieldParser(path))
                {
                    //separate data by comma
                    parser.SetDelimiters(new[] { "," });

                    //file may contain comma separated values eg."hello, world" inside quotes, 
                    //read them as one field
                    parser.HasFieldsEnclosedInQuotes = true;

                    #region Table Header

                    var header = new List<string>();
                    //consider the first line as the header
                    var headerData = parser.ReadFields();
                    if (headerData != null && headerData.Length > 0)
                    {
                        header.AddRange(headerData);
                        tableStructure.TableHeader = header;
                    }

                    #endregion

                    #region Table Body

                    var tableBody = new List<List<string>>();

                    while (!parser.EndOfData)
                    {
                        var row = new List<string>();
                        var fields = parser.ReadFields();
                        if (fields != null && fields.Length > 0)
                        {
                            row.AddRange(fields);
                            tableBody.Add(row);
                        }
                    }

                    tableStructure.TableBody = tableBody;

                    #endregion
                }

                anythingDataModel.TableStructure = tableStructure;

            }
            catch (Exception)
            {
                anythingDataModel.Error = "Sorry, Unexpected Error Occurred";
            }

            return anythingDataModel;
        }

        /// <summary>
        /// Get file path of the sample CSV file
        /// </summary>
        /// <returns>file path</returns>
        private static string GetFilePath()
        {
            var filePath = HttpContext.Current.Server.MapPath("~/Data/SampleData.csv");
            return filePath;
        }
    }
}
